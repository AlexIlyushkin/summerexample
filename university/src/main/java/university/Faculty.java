package university;

import university.model.Bachelor;
import university.model.Master;

import java.util.List;

public class Faculty {
    List<Bachelor> bachelors;
    List<Master> masters;
    List<Speciality> specialities;

    public void addSpeciality(Speciality speciality){
        specialities.add(speciality);
    }

    public Speciality getSpecialityAt(int index) throws IndexOutOfBoundsException{
        return specialities.get(index);
    }

    public Speciality getSpecialityByCode(String code) throws IllegalArgumentException{

        for(Speciality speciality : specialities){
            if(speciality.getCode().equals(code)){
                return speciality;
            }
        }

        throw new IllegalArgumentException("The department doesn't contain such speciality");
    }

    public void removeSpeciality(Speciality speciality) throws IllegalArgumentException{
        if(!specialities.remove(speciality)){
            throw new IllegalArgumentException("The department doesn't contain such speciality");
        }
    }

    public void removeSpecialityByCode(String code) throws IllegalArgumentException{
        for(Speciality speciality : specialities){
            if(speciality.getCode().equals(code)){
                specialities.remove(speciality);
                return;
            }
        }

        throw new IllegalArgumentException("The department doesn't contain such speciality");
    }

    public void removeSpecialityAt(int index) throws IndexOutOfBoundsException{
        specialities.remove(index);
    }
}
