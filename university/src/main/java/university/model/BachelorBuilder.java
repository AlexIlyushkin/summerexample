package university.model;

import university.Faculty;
import university.Group;
import university.Speciality;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;

public class BachelorBuilder extends AbstractStudentBuilder {

    @Override
    public BachelorBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @Override
    public BachelorBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @Override
    public BachelorBuilder setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    @Override
    public BachelorBuilder setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
        return this;
    }

    @Override
    public BachelorBuilder setGender(String gender) {
        this.gender = gender;
        return this;
    }

    @Override
    public BachelorBuilder setAddress(String address) {
        return null;
    }

    @Override
    public BachelorBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    @Override
    public BachelorBuilder setPassport(String passport) {
        this.passport = passport;
        return this;
    }

    @Override
    public BachelorBuilder setStudentId(String id) {
        this.studentId = id;
        return this;
    }

    @Override
    public BachelorBuilder setGroup(Group group) {
        this.group = group;
        return this;
    }

    @Override
    public BachelorBuilder setSpeciality(Speciality speciality) {
        this.speciality = speciality;
        return this;
    }

    @Override
    public BachelorBuilder setFaculty(Faculty faculty){
        this.faculty = faculty;
        return this;
    }

    public Bachelor getBachelor() throws InvocationTargetException, IllegalAccessException {
        return new Bachelor(this);
    }
}
