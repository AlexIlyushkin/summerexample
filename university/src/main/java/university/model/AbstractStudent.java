package university.model;

import university.Faculty;
import university.Group;
import university.Speciality;

public abstract class AbstractStudent extends AbstractPerson {
    private String studentId;
    private Group group;
    private Speciality speciality;
    private Faculty faculty;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        if(this.group != null){
            this.group.removeStudent(this);
        }
        this.group = group;
        group.addStudent(this);
    }

    public Speciality getSpeciality() {
        return speciality;
    }

    public void setSpeciality(Speciality speciality) {
        this.speciality = speciality;
    }

    public void setFaculty(Faculty faculty){
        this.faculty = faculty;
    }

    public Faculty getFaculty(){
        return faculty;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Student{");
        sb.append(super.toString());
        sb.append("studentId='").append(studentId).append('\'');
        sb.append(", group=").append(group);
        sb.append(", speciality=").append(speciality);
        sb.append('}');
        return sb.toString();
    }
}
