package university.model;

import university.Department;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.LocalDate;

public class ServiceStaffBuilder extends AbstractEmployeeBuilder {

    @Override
    public ServiceStaffBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @Override
    public ServiceStaffBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @Override
    public ServiceStaffBuilder setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    @Override
    public ServiceStaffBuilder setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
        return this;
    }

    @Override
    public ServiceStaffBuilder setGender(String gender) {
        this.gender = gender;
        return this;
    }

    @Override
    public ServiceStaffBuilder setAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public ServiceStaffBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    @Override
    public ServiceStaffBuilder setPassport(String passport) {
        this.passport = passport;
        return this;
    }

    @Override
    public ServiceStaffBuilder setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    @Override
    public ServiceStaffBuilder setSalary(BigDecimal salary) {
        this.salary = salary;
        return this;
    }

    @Override
    public ServiceStaffBuilder setStartDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    @Override
    public ServiceStaffBuilder setDepartment(Department department) {
        this.department = department;
        return this;
    }

    public ServiceStaff getServiceStaff() throws InvocationTargetException, IllegalAccessException {
        return new ServiceStaff(this);
    }
}
