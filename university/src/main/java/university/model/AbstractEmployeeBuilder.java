package university.model;

import university.Department;

import java.math.BigDecimal;
import java.time.LocalDate;

public abstract class AbstractEmployeeBuilder<T> extends AbstractPersonBuilder{

    protected String jobTitle;
    protected BigDecimal salary;
    protected LocalDate startDate;
    protected Department department;

    abstract T setJobTitle(String jobTitle);
    abstract T setSalary(BigDecimal salary);
    abstract T setStartDate(LocalDate startDate);
    abstract T setDepartment(Department department);

    public String getJobTitle() {
        return jobTitle;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public Department getDepartment() {
        return department;
    }
}
