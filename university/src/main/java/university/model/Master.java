package university.model;

import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;

public class Master extends AbstractStudent {

    public Master(AbstractPersonBuilder builder) throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }

    public Master(AbstractStudentBuilder builder) throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }

    public Master(MasterBuilder builder) throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }
}
