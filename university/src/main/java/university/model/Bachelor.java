package university.model;

import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;

public class Bachelor extends AbstractStudent {

    public Bachelor(AbstractPersonBuilder builder) throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }

    public Bachelor(AbstractStudentBuilder builder) throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }

    public Bachelor(BachelorBuilder builder) throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }
}
