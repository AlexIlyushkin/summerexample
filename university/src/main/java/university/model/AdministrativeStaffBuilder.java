package university.model;

import university.Department;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.LocalDate;

public class AdministrativeStaffBuilder extends AbstractEmployeeBuilder{

    @Override
    public AdministrativeStaffBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @Override
    public AdministrativeStaffBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @Override
    public AdministrativeStaffBuilder setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    @Override
    public AdministrativeStaffBuilder setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
        return this;
    }

    @Override
    public AdministrativeStaffBuilder setGender(String gender) {
        this.gender = gender;
        return this;
    }

    @Override
    public AdministrativeStaffBuilder setAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public AdministrativeStaffBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    @Override
    public AdministrativeStaffBuilder setPassport(String passport) {
        this.passport = passport;
        return this;
    }

    @Override
    public AdministrativeStaffBuilder setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    @Override
    public AdministrativeStaffBuilder setSalary(BigDecimal salary) {
        this.salary = salary;
        return this;
    }

    @Override
    public AdministrativeStaffBuilder setStartDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    @Override
    public AdministrativeStaffBuilder setDepartment(Department department) {
        this.department = department;
        return this;
    }

    public AdministrativeStaff getAdministrativeStaff() throws InvocationTargetException, IllegalAccessException {
        return new AdministrativeStaff(this);
    }
}
