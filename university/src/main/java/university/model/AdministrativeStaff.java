package university.model;

import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;

public class AdministrativeStaff extends AbstractEmployee {

    public AdministrativeStaff(AbstractEmployeeBuilder builder)
            throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }

    public AdministrativeStaff(AdministrativeStaffBuilder builder)
            throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }

    @Override
    public String toString() {
        return "" + getFirstName() + getLastName() + getMiddleName();
    }
}
