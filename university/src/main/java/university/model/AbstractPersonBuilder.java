package university.model;

import java.time.LocalDate;

public abstract class AbstractPersonBuilder<T> {

    protected String firstName;
    protected String lastName;
    protected String middleName;
    protected LocalDate birthDay;
    protected String gender;
    protected String address;
    protected String phone;
    protected String passport;

    abstract T setFirstName(String firstName);
    abstract T setLastName(String lastName);
    abstract T setMiddleName(String middleName);
    abstract T setBirthDay(LocalDate birthDay);
    abstract T setGender(String gender);
    abstract T setAddress(String address);
    abstract T setPhone(String phone);
    abstract T setPassport(String passport);

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public String getGender() {
        return gender;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassport() {
        return passport;
    }
}
