package university.model;

import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;

public class Teacher extends AbstractEmployee {

    public Teacher(AbstractPersonBuilder builder) throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }

    public Teacher(AbstractEmployeeBuilder builder) throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }

    public Teacher(TeacherBuilder builder) throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }

    @Override
    public String toString() {
        return "" + getFirstName() + getLastName() + getMiddleName();
    }
}
