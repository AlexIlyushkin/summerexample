package university.model;

import university.Faculty;
import university.Group;
import university.Speciality;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;

public class MasterBuilder extends AbstractStudentBuilder<MasterBuilder> {

    @Override
    public MasterBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @Override
    public MasterBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @Override
    public MasterBuilder setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    @Override
    public MasterBuilder setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
        return this;
    }

    @Override
    public MasterBuilder setGender(String gender) {
        this.gender = gender;
        return this;
    }

    @Override
    public MasterBuilder setAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public MasterBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    @Override
    public MasterBuilder setPassport(String passport) {
        this.passport = passport;
        return this;
    }

    @Override
    public MasterBuilder setStudentId(String id) {
        this.studentId = id;
        return this;
    }

    @Override
    public MasterBuilder setGroup(Group group) {
        this.group = group;
        return this;
    }

    @Override
    public MasterBuilder setSpeciality(Speciality speciality) {
        this.speciality = speciality;
        return this;
    }

    @Override
    public MasterBuilder setFaculty(Faculty faculty){
        this.faculty = faculty;
        return this;
    }

    public Master getMaster() throws InvocationTargetException, IllegalAccessException {
        return new Master(this);
    }
}

