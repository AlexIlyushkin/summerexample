package university.model;

import university.Department;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.LocalDate;

public class TeacherBuilder extends AbstractEmployeeBuilder {

    @Override
    public TeacherBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    @Override
    public TeacherBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @Override
    public TeacherBuilder setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    @Override
    public TeacherBuilder setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
        return this;
    }

    @Override
    public TeacherBuilder setGender(String gender) {
        this.gender = gender;
        return this;
    }

    @Override
    public TeacherBuilder setAddress(String address) {
        this.address = address;
        return this;
    }

    @Override
    public TeacherBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    @Override
    public TeacherBuilder setPassport(String passport) {
        this.passport = passport;
        return this;
    }

    @Override
    public TeacherBuilder setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    @Override
    public TeacherBuilder setSalary(BigDecimal salary) {
        this.salary = salary;
        return this;
    }

    @Override
    public TeacherBuilder setStartDate(LocalDate startDate) {
        this.startDate = startDate;
        return this;
    }

    @Override
    public TeacherBuilder setDepartment(Department department) {
        this.department = department;
        return this;
    }

    public Teacher getTeacher() throws InvocationTargetException, IllegalAccessException {
        return new Teacher(this);
    }
}
