package university.model;

import org.apache.commons.beanutils.BeanUtils;
import university.Department;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.time.LocalDate;

public abstract class AbstractEmployee extends AbstractPerson {
    private String jobTitle;
    private BigDecimal salary;
    private LocalDate startDate;
    private Department department;

    public String getJobTitle() {
        return jobTitle;
    }

    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Employee{");
        sb.append(super.toString());
        sb.append("jobTitle='").append(jobTitle).append('\'');
        sb.append(", salary=").append(salary);
        sb.append(", startDate=").append(startDate);
        sb.append(", department=").append(department);
        sb.append('}');
        return sb.toString();
    }
}
