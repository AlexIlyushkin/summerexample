package university.model;

import university.Faculty;
import university.Group;
import university.Speciality;

import java.time.LocalDate;

public abstract class AbstractStudentBuilder<T> extends AbstractPersonBuilder {

    protected String studentId;
    protected Group group;
    protected Speciality speciality;
    protected Faculty  faculty;

    abstract T setStudentId(String id);
    abstract T setGroup(Group group);
    abstract T setSpeciality(Speciality speciality);
    abstract T setFaculty(Faculty faculty);

    public String getStudentId(){
        return studentId;
    }

    public Group getGroup(){
        return group;
    }

    public Speciality getSpeciality(){
        return speciality;
    }
}
