package university.model;

import org.apache.commons.beanutils.BeanUtils;

import java.lang.reflect.InvocationTargetException;

public class ServiceStaff extends AbstractEmployee {

    public ServiceStaff(AbstractPersonBuilder builder) throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }

    public ServiceStaff(AbstractEmployeeBuilder builder) throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }

    public ServiceStaff(ServiceStaffBuilder builder) throws InvocationTargetException, IllegalAccessException {
        BeanUtils.copyProperties(this, builder);
    }

    @Override
    public String toString() {
        return "" + getFirstName() + getMiddleName() + getLastName();
    }
}
