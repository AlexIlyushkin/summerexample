package university;

import university.model.AdministrativeStaff;
import university.model.ServiceStaff;
import university.model.Teacher;

import java.util.ArrayList;
import java.util.List;

public class Department {

    private String title;
    private List<AdministrativeStaff> administrativeStaffs;
    private List<ServiceStaff> serviceStaffs;
    private List<Teacher> teachers;
    private List<Speciality> specialities;

    public Department(String title, List<Speciality> specialities){
        this.title = title;
        this.specialities = specialities;
        this.administrativeStaffs = new ArrayList<AdministrativeStaff>();
        this.serviceStaffs = new ArrayList<ServiceStaff>();
        this.teachers = new ArrayList<Teacher>();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<AdministrativeStaff> getAdministrativeStaffs(){
        return administrativeStaffs;
    }

    public void setAdministrativeStaffs(List<AdministrativeStaff> administrativeStaffs){
        this.administrativeStaffs = administrativeStaffs;
    }

    public List<ServiceStaff> getServiceStaffs(){
        return serviceStaffs;
    }

    public void setServiceStaffs(List<ServiceStaff> serviceStaffs){
        this.serviceStaffs = serviceStaffs;
    }

    public List<Teacher> getTeachers(){
        return teachers;
    }

    public void setTeachers(List<Teacher> teachers){
        this.teachers = teachers;
    }

    public void addAdministrativeStaff(AdministrativeStaff administrativeStaff){
        administrativeStaffs.add(administrativeStaff);
        administrativeStaff.setDepartment(this);
    }

    public AdministrativeStaff getAdministrativeStaffAt(int index) throws IndexOutOfBoundsException{
        return administrativeStaffs.get(index);
    }

    public void removeAdministrativeStaff(AdministrativeStaff administrativeStaff) throws IllegalArgumentException{
        if(administrativeStaffs.remove(administrativeStaff)){
            administrativeStaff.setDepartment(null);
        }else{
            throw new IllegalArgumentException("The department doesn't contain this administrative staff");
        }
    }

    public void removeAdministrativeStaffAt(int index) throws IndexOutOfBoundsException{
        if(index >= 0 && index < administrativeStaffs.size()) {
            administrativeStaffs.get(index).setDepartment(null);
            administrativeStaffs.remove(index);
        }else{
            throw new IndexOutOfBoundsException();
        }
    }

    public void addServiceStaff(ServiceStaff serviceStaff){
        serviceStaffs.add(serviceStaff);
        serviceStaff.setDepartment(this);
    }

    public ServiceStaff getServiceStaffAt(int index) throws IndexOutOfBoundsException{
        return serviceStaffs.get(index);
    }

    public void removeServiceStaff(ServiceStaff serviceStaff) throws IllegalArgumentException{
        if(serviceStaffs.remove(serviceStaff)){
            serviceStaff.setDepartment(null);
        }else{
            throw new IllegalArgumentException("The department doesn't contain this service staff");
        }
    }

    public void removeServiceStaffAt(int index) throws IndexOutOfBoundsException{
        if(index >= 0 && index < serviceStaffs.size()){
            serviceStaffs.get(index).setDepartment(null);
            serviceStaffs.remove(index);
        }else{
            throw new IndexOutOfBoundsException();
        }
    }

    public void addTeacher(Teacher teacher){
        teachers.add(teacher);
        teacher.setDepartment(this);
    }

    public Teacher getTeacherAt(int index) throws IndexOutOfBoundsException{
        return teachers.get(index);
    }

    public void removeTeacher(Teacher teacher) throws IllegalArgumentException{
        if(teachers.remove(teacher)){
            teacher.setDepartment(null);
        }else{
            throw new IllegalArgumentException("The department doesn't contain this teacher");
        }
    }

    public void removeTeacherAt(int index) throws IndexOutOfBoundsException{
        if(index >= 0 && index < teachers.size()){
            teachers.get(index).setDepartment(null);
            teachers.remove(index);
        }else{
            throw new IndexOutOfBoundsException();
        }
    }

    public void addSpeciality(Speciality speciality){
        specialities.add(speciality);
    }

    public Speciality getSpecialityAt(int index) throws IndexOutOfBoundsException{
        return specialities.get(index);
    }

    public Speciality getSpecialityByCode(String code) throws IllegalArgumentException{

        for(Speciality speciality : specialities){
            if(speciality.getCode().equals(code)){
                return speciality;
            }
        }

        throw new IllegalArgumentException("The department doesn't contain such speciality");
    }

    public void removeSpeciality(Speciality speciality) throws IllegalArgumentException{
        if(!specialities.remove(speciality)){
            throw new IllegalArgumentException("The department doesn't contain such speciality");
        }
    }

    public void removeSpecialityByCode(String code) throws IllegalArgumentException{
        for(Speciality speciality : specialities){
            if(speciality.getCode().equals(code)){
                specialities.remove(speciality);
                return;
            }
        }

        throw new IllegalArgumentException("The department doesn't contain such speciality");
    }

    public void removeSpecialityAt(int index) throws IndexOutOfBoundsException{
        specialities.remove(index);
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Department{");
        sb.append("title='").append(title).append('\'');
        //sb.append(", employees=").append(employees);
        sb.append('}');
        return sb.toString();
    }
}
