package university;

import university.model.AbstractStudent;

import java.util.ArrayList;
import java.util.List;

public class Group {
    private String groupNumber;
    private List<AbstractStudent> students;

    public Group(){
        this("");
    }

    public Group(String groupNumber){
        this(groupNumber, new ArrayList<AbstractStudent>());
    }

    public Group(String groupNumber, List<AbstractStudent> students){
        this.groupNumber = groupNumber;
        this.students = students;
    }

    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

    public List<AbstractStudent> getStudents() {
        return students;
    }

    public void setStudents(List<AbstractStudent> students) {
        this.students = students;
    }

    public void addStudent(AbstractStudent student) throws IllegalArgumentException {
        if(!students.contains(student)) {
            this.students.add(student);
            student.setGroup(this);
        }else{
            throw new IllegalArgumentException("The group already contain this student");
        }
    }

    public AbstractStudent getStudentAt(int index){
        return students.get(index);
    }

    public void removeStudent(AbstractStudent student) throws IllegalArgumentException{
        if(students.remove(student)){
            student.setGroup(null);
        }else{
            throw new IllegalArgumentException("The group doesn't contain this student");
        }
    }

    public void removeStudentAt(int index) throws IndexOutOfBoundsException{
        if(index >= 0 && index < students.size()) {
            students.get(index).setGroup(null);
            students.remove(index);
        }else{
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Group{");
        sb.append("groupNumber='").append(groupNumber).append('\'');
        sb.append(", students=").append(students);
        sb.append('}');
        return sb.toString();
    }
}
