package university;

import university.model.*;

import java.util.ArrayList;
import java.util.List;

public class University {

    private List<AdministrativeStaff> administrativeStaffs;
    private List<Bachelor> bachelors;
    private List<Master> masters;
    private List<Group> groups;
    private List<Faculty> faculties;

    public University() {
        administrativeStaffs = new ArrayList<>();
        bachelors = new ArrayList<>();
    }

    public List<AdministrativeStaff> getAdministrativeStaffs() {
        return administrativeStaffs;
    }

    public void setAdministrativeStaffs(List<AdministrativeStaff> administrativeStaffs) {
        this.administrativeStaffs = administrativeStaffs;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<Bachelor> getBachelors() {
        return bachelors;
    }

    public void setBachelors(List<Bachelor> bachelors) {
        this.bachelors = bachelors;
    }

    public void setFaculties(List<Faculty> faculties){
        this.faculties = faculties;
    }

    public List<Faculty> getFaculties(){
        return faculties;
    }

    public List<Master> getMasters(){
        return masters;
    }

    public void setMasters(List<Master> masters){
        this.masters = masters;
    }
}
